package ru.galichkin.revo.api;

import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.HttpApp;
import akka.http.javadsl.server.PathMatchers;
import akka.http.javadsl.server.Route;
import ru.galichkin.revo.api.cmd.PlaceTransferOrderCmd;
import ru.galichkin.revo.api.view.AccountListView;
import ru.galichkin.revo.api.view.AccountView;
import ru.galichkin.revo.config.json.JsonConfig;
import ru.galichkin.revo.dao.AccountRepository;
import ru.galichkin.revo.domain.OrderProcessor;

public class MoneyTransferEndpoint extends HttpApp {

    private final OrderProcessor    orderProcessor;
    private final AccountRepository accountRepository;

    public MoneyTransferEndpoint(OrderProcessor orderProcessor, AccountRepository accountRepository) {
        this.orderProcessor = orderProcessor;
        this.accountRepository = accountRepository;
    }

    @Override
    public Route routes() {

        return route(
                get(() -> pathPrefix(
                        "accounts",
                        () -> completeOK(AccountListView.create(accountRepository.findAll()), Jackson.marshaller(JsonConfig.OBJECT_MAPPER)
                        )
                )),
                get(() -> pathPrefix(
                        "account",
                        () -> path(
                                PathMatchers.segment(),
                                (String accountNumber) -> {
                                    return accountRepository.find(accountNumber)
                                            .map(acc -> completeOK(
                                                    new AccountView(acc),
                                                    Jackson.marshaller(JsonConfig.OBJECT_MAPPER)
                                                 )
                                            ).orElseGet(() -> complete(
                                                    StatusCodes.NOT_FOUND,
                                                    "Account with number " + accountNumber + " not found"
                                            ));
                                }
                        )
                    )
                ),
                path(
                        "transfers",
                        () -> post(
                                () -> entity(
                                        Jackson.unmarshaller(JsonConfig.OBJECT_MAPPER, PlaceTransferOrderCmd.class),
                                        transferOrder -> {
                                            orderProcessor.place(transferOrder);
                                            return complete(StatusCodes.ACCEPTED);
                                        }
                                )
                        )
                )
        );
    }

}

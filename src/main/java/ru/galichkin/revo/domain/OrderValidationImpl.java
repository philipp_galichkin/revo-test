package ru.galichkin.revo.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import ru.galichkin.revo.domain.entity.Account;
import ru.galichkin.revo.domain.entity.MonetaryAmount;
import ru.galichkin.revo.domain.entity.TransferOrder;

public class OrderValidationImpl implements OrderValidator {

    @Override
    public ValidationErrors validate(TransferOrder order) {
        ValidationErrors errors = new ValidationErrors();
        validateAccountFunds(order.getPayer(), order.getAmount()).ifPresent(errors::add);
        return errors;
    }

    private Optional<ValidationError> validateAccountFunds(Account payer, MonetaryAmount amount) {
        switch (payer.balance().compare(amount)) {
            case NOT_COVERED:
                return Optional.of(new ValidationError("Not enough funds"));
            case COVERED:
                return Optional.empty();
            default:
                return Optional.empty();
        }
    }
}

package ru.galichkin.revo.dao;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class GeneralInMemoryRepository<T extends Persistable<K>, K> implements BaseDao<T, K> {

    private ConcurrentHashMap<K, T> store = new ConcurrentHashMap<>();
    private ReentrantReadWriteLock  lock  = new ReentrantReadWriteLock();

    @Override
    public Optional<T> find(K id) {
        final T obj = store.get(id);
        return Optional.ofNullable(obj);
    }

    @Override
    public List<T> findAll() {
        return ImmutableList.copyOf(store.values());
    }

    @Override
    public T save(T entity) {
        try {
            lock.writeLock().tryLock(30, TimeUnit.SECONDS);
            try {
                store.put(entity.generateId(), entity);
                return store.get(entity.getId());
            } finally {
                lock.writeLock().unlock();
            }
        } catch (InterruptedException e) {
            System.err.println();
        }
        throw new RuntimeException("Unable to save an entity " + entity);
    }
}

package ru.galichkin.revo.domain.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import ru.galichkin.revo.common.Jsonable;

public class MonetaryAmount extends Jsonable {

    private final BigDecimal value;

    public MonetaryAmount(BigDecimal value) {
        Objects.requireNonNull(value, "Monetary amount value must not be null!");
        this.value = value.setScale(2, RoundingMode.HALF_EVEN);
    }

    public MonetaryAmount(double value) {
        this(BigDecimal.valueOf(value));
    }

    public BigDecimal getValue() {
        return value;
    }

    /**
     * @return true is value is greater than zero
     */
    public boolean isPositive() {
        return value.signum() > 0;
    }

    public boolean isZero() {
        return value.signum() == 0;
    }

    public ComparisonResult compare(MonetaryAmount arg) {

        Objects.requireNonNull(value, "Argument must not be null!");

        final int cmp = value.compareTo(arg.value);
        if (cmp >= 0) {
            return ComparisonResult.COVERED;
        }
        return ComparisonResult.NOT_COVERED;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MonetaryAmount)) return false;
        MonetaryAmount that = (MonetaryAmount) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public enum ComparisonResult {
        /**
         * If this monetary amount covers the argument amount, i.e. this.value >= arg.value
         */
        COVERED,
        /**
         * If this monetary amount doesn't cover the argument amount, i.e. this.value < arg.value
         */
        NOT_COVERED
    }
}

package ru.galichkin.revo.domain.entity;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;


public class AccountTransaction extends BaseUUIDKeyEntity {

    private final Account        account;
    private final Direction      direction;
    private final BigDecimal     amount;
    private final OperationState state;
    private final UUID           orderRefId;

    public AccountTransaction(Account account, Direction direction, MonetaryAmount amount, OperationState state, UUID orderRefId) {
        this._id = UUID.randomUUID();
        this.account = account;
        this.direction = direction;
        this.amount = amount.getValue();
        this.state = state;
        this.orderRefId = orderRefId;
    }

    public Account getAccount() {
        return account;
    }

    public Direction getDirection() {
        return direction;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public OperationState getState() {
        return state;
    }

    public UUID getOrderRefId() {
        return orderRefId;
    }

    public void addToAccount() {
        account.addTransaction(this);
    }

    public enum Direction {
        WITHDRAWAL,
        DEPOSIT;

        public boolean isPositive() {
            return DEPOSIT == this;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountTransaction)) return false;
        if (!super.equals(o)) return false;
        AccountTransaction that = (AccountTransaction) o;
        return Objects.equals(account, that.account) &&
                direction == that.direction &&
                Objects.equals(amount, that.amount) &&
                state == that.state &&
                Objects.equals(orderRefId, that.orderRefId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), account, direction, amount, state, orderRefId);
    }
}

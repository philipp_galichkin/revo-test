package ru.galichkin.revo.api.view;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;
import ru.galichkin.revo.domain.entity.AccountTransaction;
import ru.galichkin.revo.domain.entity.OperationState;

public class AccountTransactionView {
    private UUID                         id;
    private String                       accountNumber;
    private AccountTransaction.Direction direction;
    private BigDecimal                   amount;
    private OperationState               state;
    private UUID                         orderRefId;

    public AccountTransactionView () {}

    public AccountTransactionView(AccountTransaction origin) {
        this(
                origin.getId(),
                origin.getAccount().getAccountNumber(),
                origin.getDirection(),
                origin.getAmount(),
                origin.getState(),
                origin.getOrderRefId()
        );
    }

    public AccountTransactionView(
            UUID id,
            String accountNumber,
            AccountTransaction.Direction direction,
            BigDecimal amount,
            OperationState state,
            UUID orderRefId
    ) {
        this.id = id;
        this.accountNumber = accountNumber;
        this.direction = direction;
        this.amount = amount;
        this.state = state;
        this.orderRefId = orderRefId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public AccountTransaction.Direction getDirection() {
        return direction;
    }

    public void setDirection(AccountTransaction.Direction direction) {
        this.direction = direction;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public OperationState getState() {
        return state;
    }

    public void setState(OperationState state) {
        this.state = state;
    }

    public UUID getOrderRefId() {
        return orderRefId;
    }

    public void setOrderRefId(UUID orderRefId) {
        this.orderRefId = orderRefId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountTransactionView)) return false;
        AccountTransactionView that = (AccountTransactionView) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(accountNumber, that.accountNumber) &&
                direction == that.direction &&
                Objects.equals(amount, that.amount) &&
                state == that.state &&
                Objects.equals(orderRefId, that.orderRefId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountNumber, direction, amount, state, orderRefId);
    }

    @Override
    public String toString() {
        return "AccountTransactionView{" +
                "id=" + id +
                ", accountNumber='" + accountNumber + '\'' +
                ", direction=" + direction +
                ", amount=" + amount +
                ", state=" + state +
                ", orderRefId=" + orderRefId +
                '}';
    }
}

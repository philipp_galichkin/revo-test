package ru.galichkin.revo.dao;

import java.util.Optional;
import java.util.UUID;
import ru.galichkin.revo.domain.entity.Account;

public interface AccountRepository extends BaseDao<Account, UUID> {

    Optional<Account> find(String number);

}

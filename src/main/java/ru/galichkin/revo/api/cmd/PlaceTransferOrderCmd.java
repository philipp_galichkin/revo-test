package ru.galichkin.revo.api.cmd;

import ru.galichkin.revo.common.Jsonable;
import ru.galichkin.revo.domain.entity.MonetaryAmount;

public class PlaceTransferOrderCmd extends Jsonable {

    private String         payer;
    private String         payee;
    private MonetaryAmount amount;

    public PlaceTransferOrderCmd() {
    }

    public PlaceTransferOrderCmd(String payer, String payee, MonetaryAmount amount) {
        this.payer = payer;
        this.payee = payee;
        this.amount = amount;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public MonetaryAmount getAmount() {
        return amount;
    }

    public void setAmount(MonetaryAmount amount) {
        this.amount = amount;
    }
}

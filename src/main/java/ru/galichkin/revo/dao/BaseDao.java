package ru.galichkin.revo.dao;

import java.util.List;
import java.util.Optional;

public interface BaseDao<T extends Persistable<K>, K> {

    Optional<T> find(K id);

    List<T> findAll();

    T save(T entity);
}

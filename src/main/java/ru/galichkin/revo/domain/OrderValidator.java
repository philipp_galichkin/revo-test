package ru.galichkin.revo.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import ru.galichkin.revo.domain.entity.TransferOrder;

public interface OrderValidator {
    OrderValidationImpl.ValidationErrors validate(TransferOrder order);

    class ValidationErrors {

        public static final ValidationErrors NONE = new ValidationErrors();

        private final List<ValidationError> errors;

        public ValidationErrors() {
            errors = new ArrayList<>(1);
        }

        public ValidationErrors add(ValidationError error) {
            errors.add(error);
            return this;
        }

        public boolean hasErrors() {
            return !errors.isEmpty();
        }

        public String printMessages() {
            return errors.stream()
                    .map(ValidationError::getMessage)
                    .collect(Collectors.joining(","));
        }
    }

    class ValidationError {
        private final String message;

        public ValidationError(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}

package ru.galichkin.revo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.galichkin.revo.dao.AccountRepository;
import ru.galichkin.revo.dao.OrderRepository;
import ru.galichkin.revo.domain.OrderValidator;
import ru.galichkin.revo.domain.QueuedOrderProcessorImpl;
import ru.galichkin.revo.domain.entity.TransferOrder;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static ru.galichkin.revo.domain.OrderValidator.ValidationErrors;

@RunWith(MockitoJUnitRunner.class)
public class OrderProcessorTest {

    @Mock
    private OrderValidator validator;

    @Mock
    private OrderRepository repository;

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private QueuedOrderProcessorImpl processor;

    @Test
    public void testValidatesOrderBeforeProcessing() {
        final TransferOrder order = TransferOrder.stub();
        given(validator.validate(order)).willReturn(new ValidationErrors());

        processor.process(order);

        InOrder inOrder = inOrder(validator);
        inOrder.verify(validator).validate(order);

        verify(validator, times(1)).validate(order);
    }

    @Test
    public void testOrderIsDeclinedIfValidationFails() {
        final TransferOrder order = TransferOrder.stub();
        given(validator.validate(order)).willReturn(new ValidationErrors().add(new OrderValidator.ValidationError("Sorry")));

        final ArgumentCaptor<TransferOrder> orderRepoArgCaptor = ArgumentCaptor.forClass(TransferOrder.class);
        processor.process(order);

        verify(repository, times(2)).save(orderRepoArgCaptor.capture());
        final TransferOrder savedOrder = orderRepoArgCaptor.getValue();

        Assert.assertTrue(savedOrder.isDeclined());
    }
}

package ru.galichkin.revo.api;

import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.ContentTypes;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.testkit.JUnitRouteTest;
import akka.http.javadsl.testkit.TestRoute;
import akka.http.javadsl.testkit.TestRouteResult;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.galichkin.revo.api.cmd.PlaceTransferOrderCmd;
import ru.galichkin.revo.api.view.AccountListView;
import ru.galichkin.revo.api.view.AccountView;
import ru.galichkin.revo.config.TestApplicationConfig;
import ru.galichkin.revo.config.json.JsonConfig;

import static org.hamcrest.CoreMatchers.is;
import static ru.galichkin.revo.config.TestApplicationConfig.TestConfigParams;

public class MoneyTransferEndpointTest extends JUnitRouteTest {

    private TestRoute testRoute;

    @Before
    public void setUp() throws Exception {
        final TestApplicationConfig appConfig = new TestApplicationConfig(TestConfigParams.WITH_TEST_DATA);
        testRoute = testRoute(appConfig.testableRoutes());
    }

    @Test
    public void testTransferRequestWithoutBodyReturnsBadRequest() {
        final HttpRequest initTransferRequest = HttpRequest.POST("/transfers");
        testRoute.run(initTransferRequest).assertStatusCode(StatusCodes.BAD_REQUEST);
    }

    @Test
    public void getAccountsReturnsAccountsWithTransactions() throws Exception {
        TestRouteResult result = testRoute.run(HttpRequest.GET("/accounts"));
        result.assertStatusCode(StatusCodes.OK);

        final AccountListView responseBody = result.entity(Jackson.unmarshaller(JsonConfig.OBJECT_MAPPER, AccountListView.class));

        Assert.assertFalse(responseBody.getAccounts().isEmpty());
    }

    @Test
    public void testUserFlow() throws Exception {

        final String pupaAccNo = "00000010";
        final String lupaAccNo = "00000011";

        //Acquire accounts before transfer
        AccountView richPupa = getAccount(pupaAccNo);
        AccountView poorLupa = getAccount(lupaAccNo);

        //Assert lupa is poor but pupa is reach because of the initial transfer from Bank.
        Assert.assertTrue(richPupa.toString(), richPupa.balance().isPositive());
        Assert.assertTrue(poorLupa.toString(), poorLupa.balance().isZero());

        //Send money transfer request
        PlaceTransferOrderCmd fromPupaToLupa = new PlaceTransferOrderCmd(
                richPupa.getAccountNumber(),
                poorLupa.getAccountNumber(),
                richPupa.balance()
        );
        final HttpRequest orderRequest = HttpRequest.POST("/transfers").withEntity(
                ContentTypes.APPLICATION_JSON,
                fromPupaToLupa.asJsonString()
        );
        //Assert the order has been placed
        final TestRouteResult orderResult = testRoute.run(orderRequest);
        orderResult.assertStatusCode(StatusCodes.ACCEPTED);

        //Wait for the processing worker to complete
        Thread.sleep(2000);

        //Assert balances have changed
        AccountView poorPupa = getAccount(pupaAccNo);
        AccountView richLupa = getAccount(lupaAccNo);

        Assert.assertTrue(poorPupa.balance().isZero());
        Assert.assertThat(poorPupa.getTransactions().size(), is(2));

        Assert.assertTrue(richLupa.balance().isPositive());
        Assert.assertNotNull(richLupa.getTransactions().get(0).getOrderRefId());
    }

    private AccountView getAccount(String accountNumber) throws java.io.IOException {
        TestRouteResult   pupaResult  = testRoute.run(HttpRequest.GET("/account/" + accountNumber));
        final AccountView accountView = JsonConfig.OBJECT_MAPPER.readValue(pupaResult.entityString(), AccountView.class);
        System.out.println(accountView);
        return accountView;
    }
}

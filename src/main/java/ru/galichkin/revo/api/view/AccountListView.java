package ru.galichkin.revo.api.view;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import ru.galichkin.revo.domain.entity.Account;

public class AccountListView {

    private List<AccountView> accounts;

    public AccountListView() {
    }

    public AccountListView(List<AccountView> accounts) {
        this.accounts = accounts;
    }

    public static AccountListView create(List<Account> origin) {
        return new AccountListView(
                origin.stream()
                        .map(AccountView::new)
                        .collect(Collectors.toList())
        );
    }

    public List<AccountView> getAccounts() {
        if (accounts == null) {
            accounts = new LinkedList<>();
        }
        return accounts;
    }

    public void setAccounts(List<AccountView> accounts) {
        this.accounts = accounts;
    }

    public AccountListView addAccount(AccountView accountView) {
        getAccounts().add(accountView);
        return this;
    }

    @Override
    public String toString() {
        return "AccountListView{" +
                "accounts=" + accounts +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountListView)) return false;
        AccountListView that = (AccountListView) o;
        return Objects.equals(accounts, that.accounts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accounts);
    }
}

package ru.galichkin.revo.config;

import akka.http.javadsl.server.HttpApp;
import ru.galichkin.revo.api.MoneyTransferEndpoint;
import ru.galichkin.revo.dao.AccountInMemoryRepository;
import ru.galichkin.revo.dao.AccountRepository;
import ru.galichkin.revo.dao.OrderInMemoryRepository;
import ru.galichkin.revo.dao.OrderRepository;
import ru.galichkin.revo.domain.OrderProcessor;
import ru.galichkin.revo.domain.QueuedOrderProcessorImpl;
import ru.galichkin.revo.domain.OrderValidationImpl;
import ru.galichkin.revo.domain.OrderValidator;

public class ApplicationConfig {

    private final AccountRepository accountRepository = new AccountInMemoryRepository();
    private final OrderRepository   orderRepository   = new OrderInMemoryRepository();
    private final OrderValidator    validator         = new OrderValidationImpl();
    private final OrderProcessor    orderProcessor    = new QueuedOrderProcessorImpl(
            validator,
            orderRepository,
            accountRepository
    );

    private final MoneyTransferEndpoint moneyTransferEndpoint = new MoneyTransferEndpoint(orderProcessor, accountRepository);

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    public OrderRepository getOrderRepository() {
        return orderRepository;
    }

    public OrderValidator getValidator() {
        return validator;
    }

    public OrderProcessor getOrderProcessor() {
        return orderProcessor;
    }

    public MoneyTransferEndpoint getMoneyTransferEndpoint() {
        return moneyTransferEndpoint;
    }

    public HttpApp httpApp() {
        return moneyTransferEndpoint;
    }
}

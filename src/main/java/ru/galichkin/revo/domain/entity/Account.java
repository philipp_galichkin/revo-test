package ru.galichkin.revo.domain.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Account extends BaseUUIDKeyEntity {

    private final String                   accountNumber;
    private final String                   holderName;
    private final List<AccountTransaction> transactions;

    public Account(
            String accountNumber,
            String holderName,
            List<AccountTransaction> transactions
    ) {
        this.accountNumber = accountNumber;
        this.holderName = holderName;
        this.transactions = transactions;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getHolderName() {
        return holderName;
    }

    public List<AccountTransaction> getTransactions() {
        return transactions;
    }

    public void addTransaction(AccountTransaction transaction) {
        this.transactions.add(transaction);
    }

    public MonetaryAmount balance() {
        BigDecimal balance = BigDecimal.ZERO;
        for (AccountTransaction transaction : transactions) {
            if (transaction.getDirection().isPositive()) {
                balance = balance.add(transaction.getAmount());
            } else {
                balance = balance.subtract(transaction.getAmount());
            }
        }
        return new MonetaryAmount(balance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        if (!super.equals(o)) return false;
        Account account = (Account) o;
        return Objects.equals(accountNumber, account.accountNumber) &&
                Objects.equals(holderName, account.holderName) &&
                Objects.equals(transactions, account.transactions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), accountNumber, holderName, transactions);
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber='" + accountNumber + '\'' +
                ", holderName='" + holderName + '\'' +
                ", transactions=" + transactions +
                ", _id=" + _id +
                '}';
    }
}

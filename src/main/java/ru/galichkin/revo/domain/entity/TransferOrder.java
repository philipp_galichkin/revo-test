package ru.galichkin.revo.domain.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TransferOrder extends BaseUUIDKeyEntity {

    private final Account        payer;
    private final Account        payee;
    private final BigDecimal     amount;
    private       OperationState state       = OperationState.NEW;
    //Could be migrated into some base entity to be updated during a repository action.
    private       LocalDateTime  lastUpdated = LocalDateTime.now();
    private       String         message     = "";

    public TransferOrder(
            Account payer,
            Account payee,
            MonetaryAmount amount
    ) {
        this.payer = payer;
        this.payee = payee;
        this.amount = amount.getValue();
    }

    public Account getPayer() {
        return payer;
    }

    public Account getPayee() {
        return payee;
    }

    public MonetaryAmount getAmount() {
        return new MonetaryAmount(amount);
    }

    public AccountTransaction decline() {
        state = OperationState.DECLINED;
        lastUpdated = LocalDateTime.now();
        return new AccountTransaction(
                payee,
                AccountTransaction.Direction.WITHDRAWAL,
                getAmount(),
                state,
                getId()
        );
    }

    public void process() {
        state = OperationState.IN_PROCESS;
        lastUpdated = LocalDateTime.now();
    }

    public void updateMessage(String message) {
        this.message = message;
    }

    public boolean isDeclined() {
        return OperationState.DECLINED == state;
    }

    public List<AccountTransaction> execute() {
        state = OperationState.EXECUTED;
        lastUpdated = LocalDateTime.now();
        final List<AccountTransaction> transactions = new ArrayList<>();
        //todo account transaction factory would gracefully fit in here.
        transactions.add(new AccountTransaction(
                payer,
                AccountTransaction.Direction.WITHDRAWAL,
                getAmount(),
                state,
                getId()
        ));
        transactions.add(new AccountTransaction(
                payee,
                AccountTransaction.Direction.DEPOSIT,
                getAmount(),
                state,
                getId()
        ));
        return transactions;
    }

    public static TransferOrder stub() {
        final Account        pupa   = new Account("1", "Pupa", new ArrayList<>());
        final Account        lupa   = new Account("2", "Lupa", new ArrayList<>());
        final MonetaryAmount amount = new MonetaryAmount(BigDecimal.ONE);
        return new TransferOrder(pupa, lupa, amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransferOrder)) return false;
        TransferOrder that = (TransferOrder) o;
        return Objects.equals(payer, that.payer) &&
                Objects.equals(payee, that.payee) &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(payer, payee, amount);
    }
}

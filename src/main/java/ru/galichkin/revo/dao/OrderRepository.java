package ru.galichkin.revo.dao;

import java.util.UUID;
import ru.galichkin.revo.domain.entity.TransferOrder;

public interface OrderRepository extends BaseDao<TransferOrder, UUID> {
}

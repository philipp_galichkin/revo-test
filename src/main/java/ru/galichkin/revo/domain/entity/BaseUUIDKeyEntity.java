package ru.galichkin.revo.domain.entity;

import java.util.Objects;
import java.util.UUID;
import ru.galichkin.revo.dao.Persistable;

class BaseUUIDKeyEntity implements Persistable<UUID> {

    protected UUID _id;

    public void setId(UUID id) {
        _id = id;
    }

    @Override
    public UUID getId() {
        return _id;
    }

    @Override
    public UUID generateId() {
        _id = UUID.randomUUID();
        return _id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseUUIDKeyEntity)) return false;
        BaseUUIDKeyEntity that = (BaseUUIDKeyEntity) o;
        return Objects.equals(_id, that._id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(_id);
    }
}

package ru.galichkin.revo.domain;

import org.junit.Assert;
import org.junit.Test;
import ru.galichkin.revo.domain.entity.TransferOrder;

public class TransferOrderTest {

    @Test
    public void testOrdersAreEqualsIfTheirContentIsEquals() {
        TransferOrder instanceOne = TransferOrder.stub();
        TransferOrder instanceTwo = TransferOrder.stub();
        Assert.assertEquals(instanceOne, instanceTwo);
    }

}
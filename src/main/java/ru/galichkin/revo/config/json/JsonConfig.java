package ru.galichkin.revo.config.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import ru.galichkin.revo.domain.entity.MonetaryAmount;

public final class JsonConfig {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    static {
        OBJECT_MAPPER
                .registerModule(new JavaTimeModule())
                .findAndRegisterModules()
                .registerModule(new SerializationModule())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, true)
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true)
                .setDefaultPrettyPrinter(new DefaultPrettyPrinter())
                .configure(SerializationFeature.INDENT_OUTPUT, true)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    private static class SerializationModule extends SimpleModule {
        public SerializationModule() {
            addSerializer(new MonetaryAmountSerializer());
            addDeserializer(MonetaryAmount.class, new MonetaryAmountDeserializer());
        }

    }
}

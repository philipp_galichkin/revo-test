package ru.galichkin.revo.domain;

import ru.galichkin.revo.api.cmd.PlaceTransferOrderCmd;
import ru.galichkin.revo.domain.entity.TransferOrder;

public interface OrderProcessor {

    void process(TransferOrder order);

    void place(PlaceTransferOrderCmd transferOrder);
}

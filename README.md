## Known issues

* Lack of data validations in entities.
* There is a huge place for abstraction improvements. Current version is an imaginable "first iteration" of an agile development.
* All logging goes straight to std.out/err without any Logger abstractions.
* No transactions support
* Json timestamp output is a mess. I left it "as is" for time economy purpose.
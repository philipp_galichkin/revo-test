package ru.galichkin.revo.dao;

import java.util.UUID;
import ru.galichkin.revo.domain.entity.TransferOrder;

public class OrderInMemoryRepository extends GeneralInMemoryRepository<TransferOrder, UUID> implements OrderRepository {
}

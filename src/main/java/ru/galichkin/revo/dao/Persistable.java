package ru.galichkin.revo.dao;

public interface Persistable<K> {

    K getId();

    K generateId();
}

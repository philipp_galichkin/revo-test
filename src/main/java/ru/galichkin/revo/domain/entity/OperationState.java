package ru.galichkin.revo.domain.entity;

public enum OperationState {
    NEW,
    IN_PROCESS,
    EXECUTED,
    DECLINED,
}

package ru.galichkin.revo.dao;

import java.math.BigDecimal;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.galichkin.revo.config.TestApplicationConfig;
import ru.galichkin.revo.domain.entity.Account;
import ru.galichkin.revo.domain.entity.MonetaryAmount;
import ru.galichkin.revo.domain.entity.TransferOrder;

public class OrderRepositoryImplTest {

    private OrderRepository   orderRepository;
    private AccountRepository accountRepository;

    @Before
    public void setUp() throws Exception {
        final TestApplicationConfig testConfig = new TestApplicationConfig(TestApplicationConfig.TestConfigParams.WITH_TEST_DATA);
        orderRepository = testConfig.getOrderRepository();
        accountRepository = testConfig.getAccountRepository();
    }

    @Test
    public void findsOneOrderCreatedByDefault() {
        List<TransferOrder> presetOrders = orderRepository.findAll();
        Assert.assertEquals(1, presetOrders.size());
    }

    @Test
    public void savesOrder() {
        //For the sake of time economy, assume the test data initialized in the test application configuration
        Account       accOfPupa  = accountRepository.find("00000010").get();
        Account       accOfLupa  = accountRepository.find("00000011").get();
        TransferOrder savedOrder = orderRepository.save(new TransferOrder(accOfPupa, accOfLupa, new MonetaryAmount(BigDecimal.ONE)));
        Assert.assertNotNull(savedOrder.getId());
    }
}
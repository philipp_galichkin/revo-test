package ru.galichkin.revo.domain;

import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import ru.galichkin.revo.api.cmd.PlaceTransferOrderCmd;
import ru.galichkin.revo.dao.AccountRepository;
import ru.galichkin.revo.dao.OrderRepository;
import ru.galichkin.revo.domain.entity.Account;
import ru.galichkin.revo.domain.entity.AccountTransaction;
import ru.galichkin.revo.domain.entity.TransferOrder;

import static ru.galichkin.revo.domain.OrderValidator.ValidationErrors;

public class QueuedOrderProcessorImpl implements OrderProcessor {

    private ConcurrentLinkedQueue<TransferOrder> orderQueue = new ConcurrentLinkedQueue<>();
    private Timer                                timer      = new Timer();

    private final OrderValidator    validator;
    private final OrderRepository   orderRepository;
    private final AccountRepository accountRepository;

    public QueuedOrderProcessorImpl(
            OrderValidator validator,
            OrderRepository orderRepository,
            AccountRepository accountRepository
    ) {

        Objects.requireNonNull(validator);
        Objects.requireNonNull(orderRepository);
        Objects.requireNonNull(accountRepository);

        this.validator = validator;
        this.orderRepository = orderRepository;
        this.accountRepository = accountRepository;

        runProcessorWorker();
    }

    @Override
    public void place(PlaceTransferOrderCmd transferOrder) {
        //todo extract into a command validator
        final Account payer = accountRepository.find(transferOrder.getPayer())
                .orElseThrow(() -> new RuntimeException("Payer account not found."));
        final Account payee = accountRepository.find(transferOrder.getPayee())
                .orElseThrow(() -> new RuntimeException("Payee account not found."));
        orderQueue.add(new TransferOrder(payer, payee, transferOrder.getAmount()));
    }

    @Override
    public void process(TransferOrder order) {
        try {
            order.process();
            orderRepository.save(order);
            final ValidationErrors validationErrors = validator.validate(order);
            if (validationErrors.hasErrors()) {
                decline(order, validationErrors);
            } else {
                execute(order);
            }
        } catch (Exception e) {
            decline(order);
        }
    }

    private void execute(TransferOrder order) {
        List<AccountTransaction> executedTransactions = order.execute();
        orderRepository.save(order);
        executedTransactions.forEach(AccountTransaction::addToAccount);
        accountRepository.save(order.getPayer());
        accountRepository.save(order.getPayee());
    }

    private void decline(TransferOrder order, ValidationErrors validationErrors) {
        try {
            AccountTransaction declined = order.decline();
            final Account      payer    = order.getPayer();
            declined.addToAccount();
            if (!validationErrors.equals(ValidationErrors.NONE)) {
                order.updateMessage(validationErrors.printMessages());
            }
            orderRepository.save(order);
            accountRepository.save(payer);
        } catch (Exception e) {
            System.err.println("Sorry");
            //do something according to business requirements
        }
    }

    private void decline(TransferOrder order) {
        decline(order, ValidationErrors.NONE);
    }

    private void runProcessorWorker() {
        timer.scheduleAtFixedRate(
                new TimerTask() {
                    @Override
                    public void run() {
                        final TransferOrder order = orderQueue.poll();
                        if (order != null) {
                            process(order);
                        }
                    }
                },
                0,
                1000
        );
    }
}

package ru.galichkin.revo.config.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.math.BigDecimal;
import ru.galichkin.revo.domain.entity.MonetaryAmount;

public class MonetaryAmountDeserializer extends JsonDeserializer<MonetaryAmount> {

    @Override
    public MonetaryAmount deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return new MonetaryAmount(p.readValueAs(BigDecimal.class));
    }
}

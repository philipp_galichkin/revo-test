package ru.galichkin.revo;

import ru.galichkin.revo.config.ApplicationConfig;

public class Application {

    public static void main(String[] args) throws Exception {
        new ApplicationConfig().httpApp().startServer("localhost", 8181);
    }
}

package ru.galichkin.revo.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.galichkin.revo.config.json.JsonConfig;

public class Jsonable {

    public String asJsonString() {
        try {
            return JsonConfig.OBJECT_MAPPER.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Json generation failed with exception :(", e);
        }
    }
}

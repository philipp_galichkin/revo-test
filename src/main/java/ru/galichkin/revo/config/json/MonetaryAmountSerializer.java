package ru.galichkin.revo.config.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.math.RoundingMode;
import ru.galichkin.revo.domain.entity.MonetaryAmount;

public class MonetaryAmountSerializer extends JsonSerializer<MonetaryAmount> {
    @Override
    public void serialize(
            MonetaryAmount value, JsonGenerator gen, SerializerProvider serializers
    ) throws IOException {
        gen.writeNumber(value.getValue().setScale(2, RoundingMode.HALF_EVEN));
    }

    @Override
    public Class<MonetaryAmount> handledType() {
        return MonetaryAmount.class;
    }
}

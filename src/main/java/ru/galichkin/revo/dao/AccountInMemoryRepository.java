package ru.galichkin.revo.dao;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import ru.galichkin.revo.domain.entity.Account;

/**
 * This implementation doesn't handle situation where there are multiple accounts with the same number. To simplify the code, account
 * numbers made non-unique.
 */
public class AccountInMemoryRepository extends GeneralInMemoryRepository<Account, UUID> implements AccountRepository {

    private final ConcurrentHashMap<String, UUID> accountNumberIndex;

    public AccountInMemoryRepository() {
        accountNumberIndex = new ConcurrentHashMap<>();
    }

    public AccountInMemoryRepository(ConcurrentHashMap<String, UUID> accountNumberIndex) {
        this.accountNumberIndex = accountNumberIndex;
    }

    @Override
    public Account save(Account entity) {
        final Account account = super.save(entity);
        accountNumberIndex.putIfAbsent(account.getAccountNumber(), account.getId());
        return account;
    }

    /**
     * Lookup an account by name using local index if possible, otherwise do a sequence scan.
     *
     * @param number
     *
     * @return Optional of Account
     */
    @Override
    public Optional<Account> find(String number) {
        final UUID id = accountNumberIndex.get(number);
        if (id != null) {
            return find(id);
        }
        return findAll().stream()
                .filter(account -> account.getAccountNumber().equals(number))
                .findAny();
    }
}

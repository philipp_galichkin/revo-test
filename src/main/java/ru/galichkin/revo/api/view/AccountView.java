package ru.galichkin.revo.api.view;


import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import ru.galichkin.revo.domain.entity.Account;
import ru.galichkin.revo.domain.entity.MonetaryAmount;

public class AccountView {

    private String                       accountNumber;
    private String                       holderName;
    private BigDecimal                   balance;
    private List<AccountTransactionView> transactions = new LinkedList<>();

    public AccountView(
            String accountNumber,
            String holderName,
            BigDecimal balance,
            List<AccountTransactionView> transactions
    ) {
        this.accountNumber = accountNumber;
        this.holderName = holderName;
        this.balance = balance;
        this.transactions = transactions;
    }

    public AccountView(Account acc) {
        this(
                acc.getAccountNumber(),
                acc.getHolderName(),
                acc.balance().getValue(),
                acc.getTransactions().stream().map(AccountTransactionView::new).collect(Collectors.toList())
        );
    }

    public AccountView() {
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<AccountTransactionView> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<AccountTransactionView> transactions) {
        this.transactions = transactions;
    }

    public MonetaryAmount balance() {
        return new MonetaryAmount(balance);
    }

    @Override
    public String toString() {
        return "AccountView{" +
                "accountNumber='" + accountNumber + '\'' +
                ", holderName='" + holderName + '\'' +
                ", balance=" + balance +
                ", transactions=" + transactions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountView)) return false;
        AccountView that = (AccountView) o;
        return Objects.equals(accountNumber, that.accountNumber) &&
                Objects.equals(holderName, that.holderName) &&
                Objects.equals(balance, that.balance) &&
                Objects.equals(transactions, that.transactions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountNumber, holderName, balance, transactions);
    }
}

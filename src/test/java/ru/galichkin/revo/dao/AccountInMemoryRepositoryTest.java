package ru.galichkin.revo.dao;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import ru.galichkin.revo.domain.entity.Account;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;

public class AccountInMemoryRepositoryTest {

    private ConcurrentHashMap<String, UUID> index;
    private AccountInMemoryRepository       repository;

    @Before
    public void setUp() throws Exception {
        index = new ConcurrentHashMap<>();
        repository = Mockito.spy(new AccountInMemoryRepository(index));
    }

    @Test
    public void testSavePutsNumberInIndex() {
        final String accountNumber = "1";
        repository.save(new Account(accountNumber, "Holder", emptyList()));
        Assert.assertNotNull(index.get(accountNumber));
    }

    @Test
    public void findByNumberUsesIndexToAcquireResultIfHitsTheIndex() {
        final String  accountNumber = "1";
        final Account account       = repository.save(new Account(accountNumber, "Holder", emptyList()));
        repository.find(accountNumber);
        Mockito.verify(repository).find(account.getId());
    }

    @Test
    public void findByNumberUsesSequenceScanIfMissesTheIndex() {

        final Account account1 = new Account("1", "Holder", emptyList());
        final Account account2 = new Account("2", "Holder2", emptyList());

        given(repository.findAll()).willReturn(newArrayList(account1, account2));

        repository.find("1");

        Mockito.verify(repository, never()).find(any(UUID.class));
        Mockito.verify(repository).findAll();
    }
}
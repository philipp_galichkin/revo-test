package ru.galichkin.revo.config;

import akka.http.javadsl.server.Route;
import com.google.common.collect.Lists;
import ru.galichkin.revo.domain.entity.*;

import static ru.galichkin.revo.domain.entity.AccountTransaction.Direction.DEPOSIT;
import static ru.galichkin.revo.domain.entity.AccountTransaction.Direction.WITHDRAWAL;

public class TestApplicationConfig extends ApplicationConfig {

    public TestApplicationConfig(TestConfigParams params) {
        switch (params) {
            case PLAIN:
                break;
            case WITH_TEST_DATA:
                createTestData();
                break;
        }
    }

    private void createTestData() {
        final Account bank = getAccountRepository().save(new Account("00000001", "Bank", Lists.newArrayList()));
        final Account pupa = getAccountRepository().save(new Account("00000010", "Pupa", Lists.newArrayList()));
        final Account lupa = getAccountRepository().save(new Account("00000011", "Lupa", Lists.newArrayList()));

        final MonetaryAmount amount     = new MonetaryAmount(1000.00);
        final TransferOrder  bankToPupa = getOrderRepository().save(new TransferOrder(bank, pupa, amount));

        bank.addTransaction(new AccountTransaction(bank, WITHDRAWAL, amount, OperationState.EXECUTED, bankToPupa.getId()));
        pupa.addTransaction(new AccountTransaction(pupa, DEPOSIT, amount, OperationState.EXECUTED, bankToPupa.getId()));
    }

    public Route testableRoutes() {
        return getMoneyTransferEndpoint().routes();
    }

    public enum TestConfigParams {
        PLAIN,
        WITH_TEST_DATA
    }
}
